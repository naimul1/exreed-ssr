import axios from 'axios'
import { base } from '~/components/base'
export default {
    state: {
        courseData: '',
        course_name: ''
    },
    getters: {
        getCourseName: state => state.course_name,
    },
    mutations: {
        course_name: (state, data) => (state.course_name = data),
    },
    actions: {
        async getCourseData({ commit }, params) {
            let res = await this.$axios.post("CustomerApi/course_details", {
                course_id: params,
            });
            console.log(res);
            if (res.data.result === 1) {
                commit('course_name', res.data.data[0].course_name)
            } else {
            }
        },
    }
}