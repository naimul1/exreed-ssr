const state={
    consultencies: [
            {
              id: 1,
              name: "Consultencies Name",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              address: "Kolkata, WB",
              rating: 4,
              //designation: 'lorem',
              category: "Category",
              phone: "0123456789",
              website: "exreed.com",
            },
            {
              id: 2,
              name: "Consultencies Name",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              address: "Howrah, WB",
              rating: 3,
              category: "Ipsm dolor",
              phone: "9874561230",
              website: "exreed.com",
            },
            {
              id: 3,
              name: "Consultencies Name",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              address: "Howrah, WB",
              rating: 3,
              category: "Category",
              phone: "9874561230",
              website: "exreed.com",
            },
          ],
};
const getters={
    allconsultancies: state=>state.consultencies
};
const actions= {};
const mutations= {};
export default {state,getters,actions,mutations}