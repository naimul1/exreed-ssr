import axios from 'axios'
import { base } from '~/components/base'
export default {
    state: {
        courses: [
            {
                id: '1',
                img: 'https://www.hotrate.com/wp-content/uploads/2019/03/point-and-shoot-camera.jpg',
                courseName: 'Photography',
                courseBy: 'Filmmake Media',
                price: '750',
                rating: '4.0',
                duration: '74'
            },
            {
                id: '2',
                img: 'https://images.financialexpress.com/2019/05/cats-1182.jpg',
                courseName: 'Jewellery Design',
                courseBy: 'Object`D Art',
                price: '600',
                rating: '5.0',
                duration: '60'
            }
        ]
    },
    getters: {},
    mutations: {},
    actions: {}
}