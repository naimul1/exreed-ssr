const state={
    HRs:  [
            {
              id: 1,
              name: "Smith",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              designation: 'HR',
              company: 'Covalent Technology',
              experience: '0-2 Years',
              location: "HYD,Telangana",
              
            },
            {
              id: 2,
              name: "Jhone Doe",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              designation: 'HR',
              company: 'Covalent Technology',
              experience: '0-2 Years',
              location: "HYD,Telangana",
              
            },
            {
              id: 3,
              name: "Mahesh",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              designation: 'HR',
              company: 'Covalent Technology',
              experience: '0-2 Years',
              location: "HYD,Telangana",
            },
             {
              id: 4,
              name: "Jhone Doe",
              image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
              designation: 'HR',
              company: 'Covalent Technology',
              experience: '0-2 Years',
              location: "HYD,Telangana",
              
            },
          ],
};
const getters= {allHRs: state=>state.HRs};
const actions= {};
const mutations= {};
export default {state,getters,actions,mutations}