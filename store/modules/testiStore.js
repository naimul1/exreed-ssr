// import axios from 'axios'
// // import { base } from '~/components/base'
// // import createStore from 'vuex'

// const  testimoniales={
//     namespaced: true,
//     state: {
//         testimoniales:[
//             {
//                 id: 1,
//                 name: "Atif Aslam" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image: 'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 4,
//                 date: "20/03/2020",
//                 },
//                 {
//                     id: 2,
//                     name:"Lary Loe" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 1,
//                 date: "31/03/2021",
//                 },
//                 {
//                     id: 3,
//                     name:"Hrittik roshan" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 2,
//                 date: "21/03/2021",
//                 },
//                 {
//                     id: 4,
//                     name:"Shahrukh khan" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 3,
//                 date: "22/03/2021",
//                 },
//                 {
//                     id: 5,
//                     name:"Amir khan" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 4,
//                 date: "23/03/2021",
//                 },
//                 {
//                     id: 6,
//                     name:"Salman khan" ,
//                 info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
//                 image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
//                 rating: 5,
//                 date: "26/03/2021",
//                 }
                
//         ]
    
//     },

//     getters: {

//     },
//     mutations: {

//     },
//     actions: {
        
//     }
// }
// export default testimoniales

const state= {
  sort_by: 'date',
  sort_descending: 'true',
    testimoniallist:[
                    {
                        id: 1,
                        name: "Atif Aslam" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image: 'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 4,
                        date: ('20210314'),
                        },
                        {
                            id: 2,
                            name:"Lary Loe" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 1,
                        date: ('20200211'),
                        },
                        {
                            id: 3,
                            name:"Hrittik roshan" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 2,
                        date: ('20210313'),
                        },
                        {
                            id: 4,
                            name:"Shahrukh khan" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 3,
                        date: ('20210314'),
                        },
                        {
                            id: 5,
                            name:"Amir khan" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 4,
                        date: ('20210312'),
                        },
                        {
                            id: 6,
                            name:"Salman khan" ,
                        info: "Dramatically maintain clicks-and-mortar solutions without functional solutions.Completely synergize resource taxing relationships via premier niche markets.Professionally cultivate.",
                        image:'http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg',
                        rating: 5,
                        date: ('20210112'),
                        }                        
                ],

};
const getters= {
    alltestimonials: state=> state.testimoniallist,

        sorted_testimoniallist(state){
      const testimoniallist= [...state.testimoniallist].sort((a,b) => {
        if(a[state.sort_by] > b[state.sort_by]){return 1}
        if(a[state.sort_by] < b[state.sort_by]){return -1}
        return 0
      })
      if(!state.sort_accending){
        testimoniallist.reverse()
      }
      return testimoniallist
    }

  //   sorted_testimoniallist: function() {
  //     this.testimoniallist.sort( ( a, b) => {
  //         return new Date(a.sort_by) - new Date(b.sort_by);
  //     });
  //     return this.testimoniallist;
  // }
    // allcolleges: state=> state.colleges,
    // allcompanies: state=> state.companies,
    // allconsultancies: state=> state.consultencies,
    // alltrainer: state=> state.trainer,
    // allHRs: state=> state.HRs,
};
const actions= {
  
};
const mutations= {
  
};

export default {
    state,
    getters,
    actions,
    mutations
}
