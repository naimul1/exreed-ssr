const state={
    universities: [
        {
          id: 1,
          name: "University Name",
          image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
          address: "Kolkata, WB",
          rating: 4,
          //designation: 'lorem',
          category: "Category",
          phone: "0123456789",
          website: "exreed.com",
        },
        {
          id: 2,
          name: "University Name",
          image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
          address: "Howrah, WB",
          rating: 3,
          category: "Category",
          phone: "9874561230",
          website: "exreed.com",
        },
        {
          id: 3,
          name: "University Name",
          image: 'https://exreed.com/backend/uploads/course/1599216573.jpeg',
          address: "Howrah, WB",
          rating: 3,
          category: "Category",
          phone: "9874561230",
          website: "exreed.com",
        },
      ],
};
const getters= {
    alluniversities: state=> state.universities
};
const actions= {};
const mutations= {};
export default {
    state,getters,actions,mutations
}