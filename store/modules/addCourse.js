export default {
    state: {
        courseDValid: false,
        courseFValid: false,
        courseCValid: false,
        COURSEFAQVALID: false,
        courseTopicValid: false,
        courseTrainerValid: false,
        courseProjectValid: false,
    },
    getters: {
        getCourseDValid: state => !!state.courseDValid,
        getCourseFValid: state => !!state.courseFValid,
        getCourseCValid: state => !!state.courseCValid,
        getCourseFAqValid: state => !!state.COURSEFAQVALID,
        getCourseTopicValid: state => !!state.courseTopicValid,
        getCourseTrainerValid: state => !!state.courseTrainerValid,
        getCourseProjectValid: state => !!state.courseProjectValid
    },
    mutations: {
        COURSEDVALID: (state, courseDValid) => (state.courseDValid = !courseDValid),
        COURSEDINVALID: (state) => (state.courseDValid = false),
        COURSEfVALID: (state, courseFValid) => (state.courseFValid = !courseFValid),
        COURSEfINVALID: (state) => (state.courseFValid = false),
        COURSEcVALID: (state, courseCValid) => (state.courseCValid = !courseCValid),
        COURSEcINVALID: (state) => (state.courseCValid = false),
        COURSEfaqVALID: (state, COURSEFAQVALID) => (state.COURSEFAQVALID = !COURSEFAQVALID),
        COURSEfaqINVALID: (state) => (state.COURSEFAQVALID = false),
        COURSETOPICVALID: (state, courseTopicValid) => (state.courseTopicValid = !courseTopicValid),
        COURSETOPICINVALID: (state) => (state.courseTopicValid = false),
        COURSETRAINERVALID: (state, courseTrainerValid) => (state.courseTrainerValid = !courseTrainerValid),
        COURSETRAINERINVALID: (state) => (state.courseTrainerValid = false),
        COURSEPROJECTVALID: (state, courseProjectValid) => (state.courseProjectValid = !courseProjectValid),
        COURSETRAINERVALID: (state, courseTrainerValid) => (state.courseTrainerValid = !courseTrainerValid),
        COURSEPROJECTINVALID: (state) => (state.courseProjectValid = false),
        COURSETRAINERINVALID: (state) => (state.courseTrainerValid = false),
    },
    actions: {
        courseValid({ commit }) {
            commit('COURSEDVALID')
        },
        courseInValid({ commit }) {
            commit('COURSEDINVALID')
        },
        courseFeaturesValid({ commit }) {
            commit('COURSEfVALID')
        },
        courseFeaturesInValid({ commit }) {
            commit('COURSEfINVALID')
        },
        courseCurrValid({ commit }) {
            commit('COURSEcVALID')
        },
        courseCurrInValid({ commit }) {
            commit('COURSEcINVALID')
        },
        courseFaqValid({ commit }) {
            commit('COURSEfaqVALID')
        },
        courseFaqInValid({ commit }) {
            commit('COURSEfaqINVALID')
        },
        courseTopicValid({ commit }) {
            commit('COURSETOPICVALID')
        },
        courseTopicInValid({ commit }) {
            commit('COURSETOPICINVALID')
        },
        courseTrainerValid({ commit }) {
            commit('COURSETRAINERVALID')
        },
        courseTrainerInValid({ commit }) {
            commit('COURSETRAINERINVALID')
        },
        courseProjectValid({ commit }) {
            commit('COURSEPROJECTVALID')
        },
        courseProjectInValid({ commit }) {
            commit('COURSEPROJECTINVALID')
        },
    }
}