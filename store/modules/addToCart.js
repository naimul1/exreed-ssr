import axios from 'axios'
import { base } from '~/components/base'
export default {
    state: {
        cartCountNum: '',
    },
    getters: {
        cartCountNumber: state => state.cartCountNum,
        getCourses: state => state.courses,
    },
    mutations: {
        cartCount: (state, count) => (state.cartCountNum = count),
        addToCart: (state, count) => (state.cartCountNum++),
        substractFromCart: (state, count) => (state.cartCountNum--),
        checkedOut: (state, count) => (state.cartCountNum = 0),
    },
    actions: {
        async cartItem({ commit }) {
            let params = {
                user_id: localStorage.getItem('userId'),
                action: "List",
            }
            const response = await axios.post(`${base}CustomerApi/manage_cart`, params)

            if (response.data.message === "Cart is empty") {
                commit('cartCount', '0')
            } else {
                commit('cartCount', response.data.data.length)
            }
        },
        addToCart({ commit }) {
            commit('addToCart')
        },
        substractFromCart({ commit }) {
            commit('substractFromCart')
        },
        checkedOut({ commit }) {
            commit('checkedOut')
        },
    }
}