import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  target: 'static',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, user-scalable=no"' },
      { name: 'theme-color', content: '#ED1D25' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/fav.png' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      },
      {
        rel: 'stylesheet',
        href:
          'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'
      },
      {
        rel: 'stylesheet',
        href:
          'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Lato|Oswald&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'
      }
    ]
  },
  server: {
    port: 3334, // default: 3000
  },
  scripts: {
    dev: 'nuxt',
    generate: 'nuxt generate',
    start: 'nuxt start',
  },
  // router: {
  //   scrollBehavior: function (to, from, savedPosition) {
  //     return { x: 0, y: 0 }
  //   }
  // },
  /*
  ** Customize the progress-bar color
  */
  // loading: true,
  loading: { color: '#ED1D25', throttle: 0, },
  // loading: '~/components/loading.vue',
  /*
  ** Global CSS
  */
  css: [
    //**'@/assets/css/layout.css', 'plyr/dist/plyr.css'*/
    '@/assets/css/layout.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  //plugins: ['@/plugins/vue-fragment.js', '@/plugins/vue-plyr.js'],
  plugins: ['@/plugins/vue-fragment.js'],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // '@nuxtjs/auth'
  ],
  // auth: {
  //   strategies: {
  //     local: {
  //       endpoints: {
  //         login: { url: 'trainingInstituteApi/login', method: 'post', propertyName: 'id' },
  //         // logout: { url: '/api/auth/logout', method: 'post' },
  //         // user: { url: '/api/auth/user', method: 'get', propertyName: 'user' }
  //       },
  //       // tokenRequired: true,
  //       // tokenType: 'bearer',
  //       // globalToken: true,
  //       // autoFetchUser: true
  //     }
  //   }
  // },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    //baseURL: 'https://cors-anywhere.herokuapp.com/https://exreed.com/backend/',
    // baseURL: 'https://thingproxy.freeboard.io/fetch/https://staging.exreed.com/backend/',
    baseURL: 'https://staging.exreed.com/backend/',
    headers: {
      common: {
        'Accept': 'application/json, text/plain, Access-Control-Allow-Origin'
      },

    }
    // baseURL: 'https://exreed.com/backend/',
  },
  /*
  ** vuetify module configuration
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
