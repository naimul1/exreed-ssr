<?php
defined('BASEPATH') or exit('No direct script access allowed');
include ('application/libraries/REST_Controller.php');

class TrainingInstituteApi extends REST_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->pdo = $this->load->database('pdo', true);
        $this->load->helper('security');
        // $this->load->library('email');
        $this->load->library('upload');
        $this->load->helper(array(
            'form',
            'url',
            'common'
        ));
        $this->load->model(array(
            'TrainingInstituteApimodel',
            'Common_model',
            'Adminmodel'
        ));
        include ('application/libraries/PHPMailer/sendemail.php');
        $this->from = "surgypedia@gmail.com";
        $this->profile = FCPATH . 'uploads/profile/';
        $this->course = FCPATH . 'uploads/course/';
        $this->demo_video = FCPATH . 'uploads/demo_video/';
        $this->certification = FCPATH . 'uploads/certification/';
        $this->project = FCPATH . 'uploads/project/';
        $this->topic = FCPATH . 'uploads/topic/';
        $this->curriculum = FCPATH . 'uploads/curriculum/';
    }

    public function country_list_get($perpage = NULL, $page = NULL)
    {
        $where = "status = 1";
        $total = count($this->Common_model->getDataById('countries', $where));
        if ($perpage) {
            $limit = $perpage;
        } else {
            $limit = 6;
        }
        if ($page) {
            $offset = ($page == 1) ? 0 : (($page - 1) * $limit);
            if ($total <= $page * $limit) {
                $lastpage = 1;
            } else {
                $lastpage = 0;
            }
        } else {
            $offset = 0;
            $page = 1;
            $lastpage = 0;
        }
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $where1 = "(status = 1)"; // LIMIT $offset,$limit
        $result = $this->Common_model->getDataById('countries', $where1);
        if ($result) {
            foreach ($result as $res) {
                $listData['country_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    public function state_list_get($id)
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        
        $where = "country_id = $id AND status = 1";
        $result = $this->Common_model->getDataById('states', $where);
        if ($result) {
            foreach ($result as $res) {
                $listData['state_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    public function city_list_get($id)
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        
        $where = "state_id = $id AND status = 1";
        $result = $this->Common_model->getDataById('cities', $where);
        if ($result) {
            foreach ($result as $res) {
                $listData['city_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    public function area_list_get($id)
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        
        $where = "city_id = $id AND status = 1";
        $result = $this->Common_model->getDataById('areas', $where);
        if ($result) {
            foreach ($result as $res) {
                $listData['area_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    public function pincode_list_get($id)
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        
        $where = "area_id = $id AND status = 1";
        $result = $this->Common_model->getDataById('pincodes', $where);
        if ($result) {
            foreach ($result as $res) {
                $listData['pincode_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    function sendOTP($otp, $email)
    {
        $to = $email;
        $from = $this->from;
        $subject = "Exreed OTP";
        $body = "Dear User<br/>
        Welcome to Exreed. New content will be updated regularly. Stay tuned!<br/>
        As per your request this is your OTP: $otp <br/>
        We request you to don't share your OTP with anyone.<br/><br/>
        <b>Thanks & Regards</b><br/>
        Exreed Team.";
        $res = mailer($from, $to, $subject, $body);
        if ($res == 1) {
            return "Success";
        } else {
            return "Fail";
        }
    }

    public function get_OTP_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $email = $requeset->email;
        if (! $email) {
            $resArr['message'] = "Email id is missing";
        } else {
            $whereemail = "email = '$email'";
            $emaildata = $this->Common_model->getDataById('users', $whereemail);
            if (! empty($emaildata)) {
                $resArr['message'] = "User already exists";
            } else {
                $otp = 1234;//rand(1000, 9999);
                $result = "Success";//$this->sendOTP($otp, $email);
                if ($result == 'Success') {
                    $resArr['result'] = 1;
                    $resArr['message'] = "Success";
                    $resArr['otp'] = $otp;
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    public function forgot_password_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $email = $requeset->email;
        if (! $email) {
            $resArr['message'] = "Email id is missing";
        } else {
            $whereemail = "email = '$email'";
            $emaildata = $this->Common_model->getDataById('users', $whereemail);
            if (empty($emaildata)) {
                $resArr['message'] = "This email has not registered with us.";
            } else {
                $otp = rand(100000, 999999);
                $insertArr = array(
                    'password' => md5($otp)
                );
                $this->Common_model->update('users', $insertArr, $whereemail);
                $to = $email;
                $from = $this->from;
                $subject = "Temporary Password - Surgypedia";
                $body = "Dear User<br/>
    As per your request this is temporary password: $otp <br/><br/>
    <b>Thanks & Regards</b><br/>
    Surgypedia Team.";
                $res = 'Success';//mailer($from, $to, $subject, $body);
                if ($res == 'Success') {
                    $resArr['result'] = 1;
                    $resArr['message'] = "Success! Temporay password has been sent to your email";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    public function registration_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $compnay_name = $requeset->company_name;
        $email = $requeset->email;
        $mobile_no = $requeset->mobile_no;
        $password = $requeset->password;
        $corporate_type = $requeset->corporate_type;
        if (! $compnay_name || ! $email || ! $mobile_no || ! $password || ! $corporate_type) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $whereemail = "email = '$email'";
            $emaildata = $this->Common_model->getDataById('users', $whereemail);
            $where = "mobile_no = $mobile_no";
            $phondata = $this->Common_model->getDataById('users', $where);
            if (! empty($emaildata)) {
                $resArr['message'] = "User already exists";
            } else if (! empty($phondata)) {
                $resArr['message'] = "User already exists";
            } else {
                $insertArr = array(
                    'company_name' => $compnay_name,
                    'email' => $email,
                    'mobile_no' => $mobile_no,
                    'password' => md5($password),
                    'is_email_verified' => 1,
                    'corporate_type' => $corporate_type,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Common_model->insert('users', $insertArr);
                if ($result) {
                    $insertArr2 = array(
                        'email' => $email,
                        'mobile_no' => $mobile_no,
                        'user_id' => $result
                    );
					if($corporate_type=='Training Institute'){
                    	$this->Common_model->insert('training_institutes', $insertArr2);
					}
					if($corporate_type=='School'){
                    	$this->Common_model->insert('schools', $insertArr2);
					}
					if($corporate_type=='University'){
                    	$this->Common_model->insert('unversities', $insertArr2);
					}
					if($corporate_type=='College'){
                    	$this->Common_model->insert('colleges', $insertArr2);
					}
					if($corporate_type=='Consultancy'){
                    	$this->Common_model->insert('consultancies', $insertArr2);
					}
					if($corporate_type=='Company'){
                    	$this->Common_model->insert('company', $insertArr2);
					}
					if($corporate_type=='HR'){
                    	$this->Common_model->insert('hr', $insertArr2);
					}
					if($corporate_type=='Individual Trainer'){
                    	$this->Common_model->insert('trainers', $insertArr2);
					}
                    $to = $email;
                    $from = $this->from;
                    $subject = "Welcome to Exreed";
                    $body = "Dear User<br/>
                    Welcome to Exreed.<br/>
                    We hope that you will love the app. The preliminary content has been shared on the app to help you begin your learning journey.<br/>
                    New content will be uploaded regularly. Stay tuned! Happy studying! <br/>
                    <br/>
                    <b>Thanks & Regards</b><br/>
                    Exreed Team.";
                    // $res = mailer($from, $to, $subject, $body);
                    $resArr['result'] = 1;
                    $resArr['message'] = "Registration Successfull.";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    function login_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $email = $requeset->email;
        $password = $requeset->password;
        // $token = $requeset->fcm_token;
        // $device_type = $requeset->device_type;
        if (! $email || ! $password) {
            $resArr['message'] = "Please pass all parameters";
        } else {
            $pass = md5($password);
            if (is_numeric($email)) {
                $where = "mobile_no = '$email'";
                $phondata = $this->Common_model->getDataById('users', $where);
            } else {
                $whereemail = "email = '$email'";
                $emaildata = $this->Common_model->getDataById('users', $whereemail);
            }
            if (empty($phondata) && empty($emaildata)) {
                $resArr['message'] = "Sorry! It seems your are not registered with us. Please Sign up";
            } else {
                $where = "(email = '$email' OR mobile_no = '$email') AND password = '$pass'";
                $result = $this->Common_model->getDataById('users', $where);
                if ($result) {
                    if ($result[0]['is_email_verified'] == 0) {
                        $resArr['message'] = "Sorry! Your email has not verified yet. Please contact administrator";
                    } else {
                        /*
                         * $updateArr = array(
                         * 'fcm_token' => $token,
                         * 'device_type' => $device_type,
                         * );
                         * $this->Common_model->update('users', $updateArr, $where);
                         */
                        $listData['id'] = $result[0]['id'];
                        $listData['company_name'] = $result[0]['company_name'];
                        $listData['email'] = $result[0]['email'];
                        $listData['mobile'] = $result[0]['mobile_no'];
						$listData['corporate_type'] = $result[0]['corporate_type'];
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Invalid credential";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    function my_profile_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        if (! $user_id) {
            $resArr['message'] = "Please pass all parameters";
        } else {
            // if (is_active($user_id)) {
            $where = "id = $user_id";
            $result = $this->Common_model->getDataById('users', $where);
            $wherevid = "user_id = $user_id";
            $watcheddata = $this->Common_model->getDataById('training_institutes', $wherevid);
            if ($result) {
                $listData['user_id'] = $result[0]['id'];
                $listData['first_name'] = $watcheddata[0]['first_name'];
                $listData['sur_name'] = $watcheddata[0]['sur_name'];
                $listData['last_name'] = $watcheddata[0]['last_name'];
                $listData['email'] = $result[0]['email'];
                $listData['mobile_no'] = $result[0]['mobile_no'];
                $listData['logo_image'] = ($watcheddata[0]['logo_image']) ? base_url() . "uploads/profile/" . $watcheddata[0]['logo_image'] : "";
                $listData['address'] = unserialize($watcheddata[0]['address']);
                $listData['about_us'] = $watcheddata[0]['about_us'];
                $listData['pincode_id'] = $watcheddata[0]['pincode_id'];
                $listData['pincode'] = getName('pincodes', $watcheddata[0]['pincode_id']);
                $listData['area_id'] = $watcheddata[0]['area_id'];
                $listData['area_name'] = getName('areas', $watcheddata[0]['area_id']);
                $listData['city_id'] = $watcheddata[0]['city_id'];
                $listData['city_name'] = getName('cities', $watcheddata[0]['city_id']);
                $listData['country_id'] = $watcheddata[0]['country_id'];
                $listData['country_name'] = getName('countries', $watcheddata[0]['country_id']);
                $listData['state_id'] = $watcheddata[0]['state_id'];
                $listData['state_name'] = getName('states', $watcheddata[0]['state_id']);
                $listData['landline_no'] = $watcheddata[0]['landline_no'];
                $listData['website_link'] = $watcheddata[0]['website_link'];
                $listData['exreed_website_link'] = $watcheddata[0]['exreed_website_link'];
                $listData['open_hours'] = $watcheddata[0]['open_hours'];
                $listData['linkedin'] = $watcheddata[0]['linkedin'];
                $listData['facebook'] = $watcheddata[0]['facebook'];
                $listData['twitter'] = $watcheddata[0]['twitter'];
                $listData['instagram'] = $watcheddata[0]['instagram'];
                $listData['youtube'] = $watcheddata[0]['youtube'];
                $listData['sunday_start_time'] = $watcheddata[0]['sunday_start_time'];
                $listData['sunday_end_time'] = $watcheddata[0]['sunday_end_time'];
                $listData['monday_start_time'] = $watcheddata[0]['monday_start_time'];
                $listData['monday_end_time'] = $watcheddata[0]['monday_end_time'];
                $listData['tuesday_start_time'] = $watcheddata[0]['tuesday_start_time'];
                $listData['tuesday_end_time'] = $watcheddata[0]['tuesday_end_time'];
                $listData['wednesday_start_time'] = $watcheddata[0]['wednesday_start_time'];
                $listData['wednesday_end_time'] = $watcheddata[0]['wednesday_end_time'];
                $listData['thursday_start_time'] = $watcheddata[0]['thursday_start_time'];
                $listData['thursday_end_time'] = $watcheddata[0]['thursday_end_time'];
                $listData['friday_start_time'] = $watcheddata[0]['friday_start_time'];
                $listData['friday_end_time'] = $watcheddata[0]['friday_end_time'];
                $listData['saturday_start_time'] = $watcheddata[0]['saturday_start_time'];
                $listData['saturday_end_time'] = $watcheddata[0]['saturday_end_time'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'] = $listData;
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "No data found";
            }
            /*
             * } else {
             * $resArr['message'] = "Your account is inactive. Please contact with administration";
             * }
             */
        }
        echo $this->response($resArr, 200);
    }

    function update_profile_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $firstname = $requeset->first_name;
        $surname = $requeset->sur_name;
        $lastname = $requeset->last_name;
        $email = $requeset->email;
        $mobile_no = $requeset->mobile_no;
        $landline_no = $requeset->landline_no;
        $country = $requeset->country_id;
        $state = $requeset->state_id;
        $city = $requeset->city_id;
        $area = $requeset->area_id;
        $pincode = $requeset->pincode_id;
        $address = $requeset->address;
        $website = $requeset->website;
        $exreed_website = $requeset->exreed_website;
        $aboutus = $requeset->aboutus;
        $linkedin = $requeset->linkedin;
        $facebook = $requeset->facebook;
        $twitter = $requeset->twitter;
        $instagram = $requeset->instagram;
        $youtube = $requeset->youtube;
        $sunday_start_time = $requeset->sunday_start_time;
        $sunday_end_time = $requeset->sunday_end_time;
        $monday_start_time = $requeset->monday_start_time;
        $monday_end_time = $requeset->monday_end_time;
        $tuesday_start_time = $requeset->tuesday_start_time;
        $tuesday_end_time = $requeset->tuesday_end_time;
        $wednesday_start_time = $requeset->wednesday_start_time;
        $wednesday_end_time = $requeset->wednesday_end_time;
        $thursday_start_time = $requeset->thursday_start_time;
        $thursday_end_time = $requeset->thursday_end_time;
        $friday_start_time = $requeset->friday_start_time;
        $friday_end_time = $requeset->friday_end_time;
        $saturday_start_time = $requeset->saturday_start_time;
        $saturday_end_time = $requeset->saturday_end_time;
        $image = $requeset->logo_image;
        $open_hours = $requeset->open_hours;
        if (! $user_id) {
            $resArr['message'] = "Please pass all parameters";
        } else {
            // if (is_active($user_id)) {
            if ($email) {
                $whereemail = "email = '$email' AND id != $user_id";
                $emaildata = $this->Common_model->getDataById('users', $whereemail);
            }
            if ($mobile_no) {
                $where = "mobile_no = $mobile_no AND id != $user_id";
                $phondata = $this->Common_model->getDataById('users', $where);
            }
            if (! empty($emaildata)) {
                $resArr['message'] = "A user with this email has already exist";
            } else if (! empty($phondata)) {
                $resArr['message'] = "A user with this mobile no has already exist";
            } else {
                if ($image) {
                    $data = str_replace('data:image/jpeg;base64,', '', $image);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = FCPATH . '/uploads/profile/';
                    $uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
                    $filetosave = FCPATH . '/uploads/profile/' . $uploadedimgProfile;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                }
                $whereid = "user_id = $user_id";
                $profiledata = $this->Common_model->getDataById('training_institutes', $whereid);
                if (! empty($profiledata)) {
                    $updateArr = array(
                        'first_name' => $firstname,
                        'sur_name' => $surname,
                        'last_name' => $lastname,
                        'email' => $email,
                        'mobile_no' => $mobile_no,
                        'landline_no' => $landline_no,
                        'logo_image' => ($image) ? $uploadedimgProfile : $profiledata[0]['logo_image'],
                        'country_id' => $country,
                        'state_id' => $state,
                        'city_id' => $city,
                        'area_id' => $area,
                        'pincode_id' => $pincode,
                        'address' => serialize($address),
                        'website_link' => $website,
                        'exreed_website_link' => $exreed_website,
                        'open_hours' => $open_hours,
                        'about_us' => $aboutus,
                        'linkedin' => $linkedin,
                        'facebook' => $facebook,
                        'twitter' => $twitter,
                        'instagram' => $instagram,
                        'youtube' => $youtube,
                        'sunday_start_time' => $sunday_start_time,
                        'sunday_end_time' => $sunday_end_time,
                        'monday_start_time' => $monday_start_time,
                        'monday_end_time' => $monday_end_time,
                        'tuesday_start_time' => $tuesday_start_time,
                        'tuesday_end_time' => $tuesday_end_time,
                        'wednesday_start_time' => $wednesday_start_time,
                        'wednesday_end_time' => $wednesday_end_time,
                        'thursday_start_time' => $thursday_start_time,
                        'thursday_end_time' => $thursday_end_time,
                        'friday_start_time' => $friday_start_time,
                        'friday_end_time' => $friday_end_time,
                        'saturday_start_time' => $saturday_start_time,
                        'saturday_end_time' => $saturday_end_time
                    );
                    $where_update = "user_id=$user_id";
                    $result = $this->Common_model->update('training_institutes', $updateArr, $where_update);
                }
                if ($result) {
                    $updateArr2 = array(
                        'email' => $email,
                        'mobile_no' => $mobile_no
                    );
                    $where_update2 = "id=$user_id";
                    $result = $this->Common_model->update('users', $updateArr2, $where_update2);
                    $resArr['result'] = 1;
                    $resArr['message'] = "Success. Your profile has been updated successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
            /*
             * } else {
             * $resArr['message'] = "Your account is inactive. Please contact with administration";
             * }
             */
        }
        echo $this->response($resArr, 200);
    }

    function category_list_get()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $maincatdata = $this->Common_model->getTableData('categories');
        if ($maincatdata) {
            foreach ($maincatdata as $detail) {
                $listData['id'] = $detail['id'];
                $listData['name'] = $detail['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "Success.";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['message'] = "No data found";
        }
        echo $this->response($resArr, 200);
    }

    function sub_category_list_get($id)
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $where = "(category_id = $id)";
        $maincatdata = $this->Common_model->getDataById('sub_categories', $where);
        if ($maincatdata) {
            foreach ($maincatdata as $detail) {
                $listData['id'] = $detail['id'];
                $listData['name'] = $detail['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "Success.";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['message'] = "No data found";
        }
        echo $this->response($resArr, 200);
    }

    function add_course_details_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_name = $requeset->course_name;
        $duration = $requeset->duration;
        $language_id = $requeset->language_id;
        $certificate_image = $requeset->certification;
        $eligibility = $requeset->eligibility;
        $demo_vidoe = $requeset->demo_video;
        $category_id = $requeset->category_id;
        $sub_category_id = $requeset->sub_category_id;
        $fee = $requeset->fee;
        $description = $requeset->description;
        $why = $requeset->why_this;
        $image = $requeset->image;
        if (! $course_name || ! $user_id || ! $duration || ! $language_id || ! $certificate_image || ! $eligibility || ! $demo_vidoe || ! $fee || ! $description || ! $why || ! $category_id || ! $sub_category_id || ! $duration || ! $image) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $certificate_image_upload = "";
            $demo_video_upload = "";
            if ($certificate_image) {
                $explodedstring = explode(',', $certificate_image);
                $explodedstring1 = explode('/', $explodedstring[0]);
                $explodedstring2 = explode(';', $explodedstring1[1]);
                $extension = $explodedstring2[0];
                $data = str_replace('data:image/' . $extension . ';base64,', '', $certificate_image);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = FCPATH . '/uploads/certification/';
                $certificate_image_upload = strtotime(date("Y-m-d H:i:s")) . ".$extension";
                $filetosave = FCPATH . '/uploads/certification/' . $certificate_image_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            if ($image) {
                $explodedimage = explode(',', $image);
                $explodedimage1 = explode('/', $explodedimage[0]);
                $explodedimage2 = explode(';', $explodedimage1[1]);
                $extensionimage = $explodedimage2[0];
                $data = str_replace('data:image/' . $extensionimage . ';base64,', '', $image);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = FCPATH . '/uploads/course/';
                $image_upload = strtotime(date("Y-m-d H:i:s")) . ".$extensionimage";
                $filetosave = FCPATH . '/uploads/course/' . $image_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            if ($demo_vidoe) {
                $data = str_replace('data:video/mp4;base64,', '', $demo_vidoe);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = $this->demo_video;
                $demo_video_upload = strtotime(date("Y-m-d H:i:s")) . ".mp4";
                $filetosave = FCPATH . '/uploads/demo_video/' . $demo_video_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            $insertArr = array(
                'user_id' => $user_id,
                'course_name' => $course_name,
                'duration' => $duration,
                'language_id' => $language_id,
                'certification' => $certificate_image_upload,
                'image' => $image_upload,
                'category_id' => $category_id,
                'sub_category_id' => $sub_category_id,
                'eligibility' => $eligibility,
                'fee' => $fee,
                'demo_video' => $demo_video_upload,
                'description' => $description,
                'why_this' => $why,
                'created_at' => date("Y-m-d H:i")
            );
            // print_r($insertArr);die;
            $result = $this->Common_model->insert('course_details', $insertArr);
            if ($result) {
                $where = "user_id = $user_id";
                $topicdata = $this->Common_model->getDataById('course_details', $where);
                $resArr['result'] = 1;
                $resArr['course_id'] = $result;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course has been added successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_course_feature_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $description = json_encode($requeset->description);
        if (! $course_id || ! $user_id || ! $description) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $desc = json_decode($description);
            foreach ($desc as $des) {
                $insertArr = array(
                    'user_id' => $user_id,
                    'course_id' => $course_id,
                    'description' => $des->description,
                    'created_at' => date("Y-m-d H:i")
                );
                $result = $this->Common_model->insert('course_features', $insertArr);
            }
            if ($result) {
                $where = "course_id = $course_id";
                $topicdata = $this->Common_model->getDataById('course_features', $where);
                $resArr['result'] = 1;
                $resArr['course_id'] = $course_id;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course Feature has been added successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_course_faq_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $question = json_encode($requeset->question);
        if (! $course_id || ! $user_id || ! $question) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $question_decode = json_decode($question);
            foreach ($question_decode as $quedecode) {
                $insertArr = array(
                    'user_id' => $user_id,
                    'course_id' => $course_id,
                    'question' => $quedecode->question,
                    'answer' => $quedecode->answer,
                    'created_at' => date("Y-m-d H:i")
                );
                $result = $this->Common_model->insert('course_faqs', $insertArr);
            }
            if ($result) {
                $where = "course_id = $course_id";
                $topicdata = $this->Common_model->getDataById('course_faqs', $where);
                $resArr['result'] = 1;
                $resArr['course_id'] = $course_id;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course FAQs has been added successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_course_project_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $name = json_encode($requeset->name);
        if (! $course_id || ! $user_id || ! $name) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $name_decode = json_decode($name);
            foreach ($name_decode as $nmdec) {
                $explodedimage = explode(',', $nmdec->image);
                $explodedimage1 = explode('/', $explodedimage[0]);
                $explodedimage2 = explode(';', $explodedimage1[1]);
                $extensionimage = $explodedimage2[0];
                $data = str_replace('data:image/' . $extensionimage . ';base64,', '', $nmdec->image);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = $this->project;
                $pdf_file = strtotime(date("Y-m-d H:i:s")) . $i . ".$extensionimage";
                $filetosave = $this->project . "" . $pdf_file;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
                $insertArr = array(
                    'user_id' => $user_id,
                    'course_id' => $course_id,
                    'name' => $nmdec->name,
                    'image' => $pdf_file,
                    'created_at' => date("Y-m-d H:i")
                );
                $result = $this->Common_model->insert('course_projects', $insertArr);
                $i ++;
            }
            if ($result) {
                $where = "course_id = $course_id";
                $topicdata = $this->Common_model->getDataById('course_projects', $where);
                $resArr['result'] = 1;
                $resArr['course_id'] = $course_id;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course Project has been added successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_course_topic_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $name = json_encode($requeset->name);
        if (! $course_id || ! $user_id || ! $name) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $name_decode = json_decode($name);
            foreach ($name_decode as $nmdec) {
                $data = str_replace('data:video/mp4;base64,', '', $nmdec->image);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = $this->topic;
                $pdf_file = strtotime(date("Y-m-d H:i:s")) . $i . ".mp4";
                $filetosave = $this->topic . "" . $pdf_file;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
                $insertArr = array(
                    'user_id' => $user_id,
                    'course_id' => $course_id,
                    'name' => $nmdec->name,
                    'image' => $pdf_file,
                    'created_at' => date("Y-m-d H:i")
                );
                $result = $this->Common_model->insert('course_topics', $insertArr);
                $i ++;
            }
            if ($result) {
                $where = "course_id = $course_id";
                $topicdata = $this->Common_model->getDataById('course_topics', $where);
                foreach ($topicdata as $res) {
                    $course_id = $res['id'];
                    $listData['topic_id'] = $res['id'];
                    $listData['name'] = $res['name'];
                    $listData['video_file'] = base_url() . 'uploads/topic/' . $res['image'];
                    $curriculmdata[] = $listData;
                }
                $resArr['result'] = 1;
                $resArr['course_id'] = $course_id;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course Topic has been added successfully";
                $resArr['data'] = $curriculmdata;
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_course_curriculum_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $name = json_encode($requeset->name);
        if (! $course_id || ! $user_id || ! $name) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $name_decode = json_decode($name);
            foreach ($name_decode as $nmdec) {
                $config['upload_path'] = $this->curriculum;
                $data = str_replace('data:application/pdf;base64,', '', $nmdec->pdf_file);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = $this->curriculum;
                $pdf_file = strtotime(date("Y-m-d H:i:s")) . $i . ".pdf";
                $filetosave = $this->curriculum . "" . $pdf_file;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
                $insertArr = array(
                    'user_id' => $user_id,
                    'course_id' => $course_id,
                    'name' => $nmdec->name,
                    'pdf_file' => $pdf_file,
                    'created_at' => date("Y-m-d H:i")
                );
                $result = $this->Common_model->insert('course_curriculums', $insertArr);
            }
            if ($result) {
                $where = "course_id = $course_id AND user_id = $user_id";
                $topicdata = $this->Common_model->getDataById('course_curriculums', $where);
                foreach ($topicdata as $res) {
                    $course_id = $res['id'];
                    $listData['curriculum_id'] = $res['id'];
                    $listData['name'] = $res['name'];
                    $listData['pdf_file'] = base_url() . 'uploads/curriculum/' . $res['pdf_file'];
                    $curriculmdata[] = $listData;
                }
                $resArr['result'] = 1;
                $resArr['course_id'] = $course_id;
                $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                $resArr['message'] = "Course Curriculum has been added successfully";
                $resArr['data'] = $curriculmdata;
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Something went wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function add_trainer_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $name = $requeset->name;
        $email = $requeset->email;
        $image_file = $requeset->image_file;
        $designation = $requeset->designation;
        $description = $requeset->description;
        $facebook = $requeset->facebook;
        $youtube = $requeset->youtube;
        $twitter = $requeset->twitter;
        $instagram = $requeset->instagram;
        if (! $course_id || ! $user_id || ! $name || ! $image_file || ! $email || ! $designation || ! $description) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $whereemail = "email = '$email'";
            $emaildata = $this->Common_model->getDataById('users', $whereemail);
            if (! empty($emaildata)) {
                $resArr['message'] = "User already exists";
            } else {
                $image = "";
                if ($image_file) {
                    $explodedimage = explode(',', $image_file);
                    $explodedimage1 = explode('/', $explodedimage[0]);
                    $explodedimage2 = explode(';', $explodedimage1[1]);
                    $extensionimage = $explodedimage2[0];
                    $data = str_replace('data:image/' . $extensionimage . ';base64,', '', $image_file);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = $this->profile;
                    $image = strtotime(date("Y-m-d H:i:s")) . ".$extensionimage";
                    $filetosave = $this->profile . "" . $image;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                }
                $insertArr = array(
                    'company_name' => $name,
                    'email' => $email,
                    'mobile_no' => "",
                    'password' => "",
                    'is_email_verified' => 0,
                    'corporate_type' => "Trainer",
                    'created_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Common_model->insert('users', $insertArr);
                if ($result) {
                    $insertArr2 = array(
                        'email' => $email,
                        'name' => $name,
                        'image' => $image,
                        'designation' => $designation,
                        'description' => $description,
                        'facebook' => ($facebook) ? $facebook : "",
                        'twitter' => ($twitter) ? $twitter : "",
                        'youtube' => ($youtube) ? $youtube : "",
                        'instagram' => ($instagram) ? $instagram : "",
                        'mobile_no' => "",
                        'user_id' => $result,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $result2 = $this->Common_model->insert('trainers', $insertArr2);
                    $insertArr3 = array(
                        'trainer_id' => $result2,
                        'training_institute_user_id' => $user_id,
                        'course_id' => $course_id,
                        'trainer_user_id' => $result,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert('training_institute_trainers', $insertArr3);
                    $where = "course_id = $course_id AND training_institute_user_id = $user_id";
                    $topicdata = $this->Common_model->getDataById('training_institute_trainers', $where);
                    $resArr['result'] = 1;
                    $resArr['course_id'] = $course_id;
                    $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                    $resArr['message'] = "Trainer has been added successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

	public function add_trainer_without_course_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $name = $requeset->name;
        $email = $requeset->email;
        $image_file = $requeset->image_file;
        $designation = $requeset->designation;
        $description = $requeset->description;
        $facebook = $requeset->facebook;
        $youtube = $requeset->youtube;
        $twitter = $requeset->twitter;
        $instagram = $requeset->instagram;
        if (! $user_id || ! $name || ! $image_file || ! $email || ! $designation || ! $description) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $whereemail = "email = '$email'";
            $emaildata = $this->Common_model->getDataById('users', $whereemail);
            if (! empty($emaildata)) {
                $resArr['message'] = "User already exists";
            } else {
                $image = "";
                if ($image_file) {
                    $explodedimage = explode(',', $image_file);
                    $explodedimage1 = explode('/', $explodedimage[0]);
                    $explodedimage2 = explode(';', $explodedimage1[1]);
                    $extensionimage = $explodedimage2[0];
                    $data = str_replace('data:image/' . $extensionimage . ';base64,', '', $image_file);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = $this->profile;
                    $image = strtotime(date("Y-m-d H:i:s")) . ".$extensionimage";
                    $filetosave = $this->profile . "" . $image;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                }
                $insertArr = array(
                    'company_name' => $name,
                    'email' => $email,
                    'mobile_no' => "",
                    'password' => "",
                    'is_email_verified' => 0,
                    'corporate_type' => "Trainer",
                    'created_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Common_model->insert('users', $insertArr);
                if ($result) {
                    $insertArr2 = array(
                        'email' => $email,
                        'name' => $name,
                        'image' => $image,
                        'designation' => $designation,
                        'description' => $description,
                        'facebook' => ($facebook) ? $facebook : "",
                        'twitter' => ($twitter) ? $twitter : "",
                        'youtube' => ($youtube) ? $youtube : "",
                        'instagram' => ($instagram) ? $instagram : "",
                        'mobile_no' => "",
                        'user_id' => $result,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $result2 = $this->Common_model->insert('trainers', $insertArr2);
                    $insertArr3 = array(
                        'trainer_id' => $result2,
                        'training_institute_user_id' => $user_id,
                        'trainer_user_id' => $result,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Common_model->insert('training_institute_trainers', $insertArr3);
                    $where = "training_institute_user_id = $user_id";
                    $topicdata = $this->Common_model->getDataById('training_institute_trainers', $where);
                    $resArr['result'] = 1;
                    $resArr['course_id'] = $course_id;
                    $resArr['count'] = ! empty($topicdata) ? count($topicdata) : 0;
                    $resArr['message'] = "Trainer has been added successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    public function assign_trainer_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $trainer_id = $requeset->trainer_id;
        if (! $course_id || ! $user_id || ! $trainer_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $trainer_id";
            $trainerdata = $this->Common_model->getDataById('trainers', $where);
            if (! empty($trainerdata)) {
                $trainer_user_id = $trainerdata[0]['user_id'];
                $insertArr3 = array(
                    'trainer_id' => $trainer_id,
                    'training_institute_user_id' => $user_id,
                    'course_id' => $course_id,
                    'trainer_user_id' => $trainer_user_id,
                    'created_at' => date('Y-m-d H:i:s')
                );
                $result = $this->Common_model->insert('training_institute_trainers', $insertArr3);
                if ($result) {
                    $resArr['result'] = 1;
                    $resArr['course_id'] = $course_id;
                    $resArr['message'] = "Trainer has been assigned successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Trainer details not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    public function trainer_list_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        if (! $user_id) {
            $resArr['message'] = "Please pass all parameter";
        } else {
            $where = "training_institute_user_id = $user_id";
            $result = $this->Common_model->getDataById('training_institute_trainers', $where);
            if ($result) {
                foreach ($result as $res) {
                    $trainer_id = $res['trainer_id'];
                    $whereTrainer = "id = $trainer_id";
                    $trainerdata = $this->Common_model->getDataById('trainers', $whereTrainer);
                    $listData['trainer_id'] = $trainer_id;
                    $listData['name'] = $trainerdata[0]['name'];
                    $listData['designation'] = $trainerdata[0]['designation'];
                    $listData['image'] = ($trainerdata[0]['image']) ? base_url(). "uploads/profile/" . $trainerdata[0]['image']:"";
                    $listData['description'] = ($trainerdata[0]['description']) ? $trainerdata[0]['description'] :"";
                    $listData['facebook'] = ($trainerdata[0]['facebook']) ? $trainerdata[0]['facebook'] : "";
                    $listData['twitter'] = ($trainerdata[0]['twitter']) ? $trainerdata[0]['twitter'] : "";
                    $listData['linkedin'] = ($trainerdata[0]['linkedin']) ? $trainerdata[0]['linkedin'] : "";
                    $listData['youtube'] = ($trainerdata[0]['youtube']) ? $trainerdata[0]['youtube'] : "";
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function generate_invoice_number()
    {
        $whereuseduser = "(1=1) ORDER BY id DESC LIMIT 0,1";
        $promousedUserdata = $this->Common_model->getDataById('purchased_subscriptions', $whereuseduser);
        if (! empty($promousedUserdata)) {
            $id = $promousedUserdata[0]['id'];
            if (strlen($id) < 2) {
                if ($id == 9) {
                    $invoice_number = "SP/0" . ($id + 1) . "/20-21";
                } else {
                    $invoice_number = "SP/00" . ($id + 1) . "/20-21";
                }
            } else if (strlen($id) > 1 && strlen($id) < 3) {
                if ($id == 99) {
                    $invoice_number = "SP/" . ($id + 1) . "/20-21";
                } else {
                    $invoice_number = "SP/0" . ($id + 1) . "/20-21";
                }
            } else {
                $invoice_number = "SP/" . $id . "/20-21";
            }
        } else {
            $invoice_number = "SP/001/20-21";
        }
        return $invoice_number;
    }

    public function do_upload_video($path)
    {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'mov|mpeg|mp3|avi|mp4';
        $config['max_size'] = 10000000;
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = FALSE;
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if (! $this->upload->do_upload('file')) {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            return $error;
        } else {
            $upload_data = $this->upload->data();
            $video_path = $upload_data['full_path'];
            $data = array(
                'upload_data' => $upload_data
            );
            return $data;
        }
    }

    public function do_upload($path)
    {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        
        if (! $this->upload->do_upload('file')) {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            
            return $error;
        } else {
            $data = array(
                'upload_data' => $this->upload->data()
            );
            return $data;
        }
    }

    public function language_list_get()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $where = "1 = 1";
        $result = $this->Common_model->getDataById('languages', $where);
        if ($result) {
            foreach ($result as $res) {
                $listData['language_id'] = $res['id'];
                $listData['name'] = $res['name'];
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            }
        } else {
            $resArr['result'] = 0;
            $resArr['message'] = "Failed";
        }
        echo $this->response($resArr, 200);
    }

    function course_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $perpage = $requeset->per_page;
        $page = $requeset->page_no;
        $resArr['result'] = 0;
        $resArr['message'] = "";
        if (! $user_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "(user_id = $user_id)";
            $total = count($this->Common_model->getDataById('course_details', $where));
            if ($perpage) {
                $limit = $perpage;
            } else {
                $limit = 6;
            }
            if ($page) {
                $offset = ($page == 1) ? 0 : (($page - 1) * $limit);
                if ($total <= $page * $limit) {
                    $lastpage = 1;
                } else {
                    $lastpage = 0;
                }
            } else {
                $offset = 0;
                $page = 1;
                $lastpage = 0;
            }
            $where1 = "(user_id = $user_id) "; // LIMIT $offset,$limit
            $result = $this->Common_model->getDataById('course_details', $where1);
            if ($result) {
                foreach ($result as $res) {
                    $course_id = $res['id'];
                    $training_institute_id = $res['user_id'];
                    $wheretrainer = "course_id = $course_id AND training_institute_user_id = $user_id";
                    $trainingdata = $this->Common_model->getDataById('training_institute_trainers', $wheretrainer);
                    $trainer_id = (! empty($trainingdata)) ? $trainingdata[0]['trainer_id'] : "";
                    $listData['course_id'] = $res['id'];
                    $listData['name'] = $res['course_name'];
                    $listData['trainer_name'] = getName('trainers', $trainer_id);
                    $listData['fee'] = $res['fee'];
                    $listData['duration'] = $res['duration'];
                    $listData['image'] = base_url() . 'uploads/course/' . $res['image'];
                    $listData['status'] = $res['status'];
                    $listData['rating'] = 0;
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    // $resArr['per_page'] = $limit;
                    // $resArr['page_no'] = $page;
                    // $resArr['total'] =$total;
                    // $resArr['last_page'] = $lastpage;
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_curriculum_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "user_id = $user_id AND course_id = $course_id";
            $result = $this->Common_model->getDataById('course_curriculums', $where);
            if ($result) {
                foreach ($result as $res) {
                    $course_id = $res['id'];
                    $training_institute_id = $res['user_id'];
                    $wheretrainer = "course_id = $course_id";
                    $trainingdata = $this->Common_model->getDataById('training_institute_trainers', $wheretrainer);
                    $trainer_id = (! empty($trainingdata)) ? $trainingdata[0]['trainer_id'] : "";
                    $listData['curriculum_id'] = $res['id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['name'] = $res['name'];
                    $listData['pdf_file'] = base_url() . 'uploads/curriculum/' . $res['pdf_file'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_feature_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "user_id = $user_id AND course_id = $course_id";
            $result = $this->Common_model->getDataById('course_features', $where);
            if ($result) {
                foreach ($result as $res) {
                    $listData['curriculum_id'] = $res['id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['description'] = $res['description'];
                    // $listData['pdf_file'] = base_url().'uploads/curriculum/'.$res['pdf_file'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_topic_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "user_id = $user_id AND course_id = $course_id";
            $result = $this->Common_model->getDataById('course_topics', $where);
            if ($result) {
                foreach ($result as $res) {
                    $listData['curriculum_id'] = $res['id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['name'] = $res['name'];
                    $listData['video_file'] = base_url() . 'uploads/topic/' . $res['image'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_project_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "user_id = $user_id AND course_id = $course_id";
            $result = $this->Common_model->getDataById('course_projects', $where);
            if ($result) {
                foreach ($result as $res) {
                    $listData['curriculum_id'] = $res['id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['name'] = $res['name'];
                    $listData['pdf_file'] = base_url() . 'uploads/project/' . $res['image'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_faq_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "user_id = $user_id AND course_id = $course_id";
            $result = $this->Common_model->getDataById('course_faqs', $where);
            if ($result) {
                foreach ($result as $res) {
                    $listData['curriculum_id'] = $res['id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['question'] = $res['question'];
                    $listData['answer'] = $res['answer'];
                    // $listData['pdf_file'] = base_url().'uploads/project/'.$res['image'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_trainer_list_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "course_id = $course_id AND training_institute_user_id = $user_id";
            $result = $this->Common_model->getDataById('training_institute_trainers', $where);
            if ($result) {
                foreach ($result as $res) {
                    $trainer_id = $res['trainer_id'];
                    $wheretrainer = "id = $trainer_id";
                    $trainingdata = $this->Common_model->getDataById('trainers', $wheretrainer);
                    $listData['trainer_id'] = $res['trainer_id'];
                    $listData['course_id'] = $res['course_id'];
                    $listData['trainer_name'] = getName('trainers', $res['trainer_id']);
                    $listData['email'] = $trainingdata[0]['email'];
                    $listData['mobile'] = $trainingdata[0]['mobile'];
                    $listData['designation'] = $trainingdata[0]['designation'];
                    $listData['description'] = $trainingdata[0]['description'];
                    $listData['image'] = base_url() . 'uploads/profile/' . $trainingdata[0]['image'];
                    $resArr['result'] = 1;
                    $resArr['message'] = "success";
                    $resArr['data'][] = $listData;
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function course_details_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        if (! $user_id || ! $course_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $course_id AND user_id = $user_id";
            $course_details = $this->Common_model->getDataById('course_details', $where);
            if ($course_details) {
                $listData['course_id'] = $course_details[0]['id'];
                $listData['course_name'] = $course_details[0]['course_name'];
                $listData['training_institute_name'] = getCompanyName('users', $course_details[0]['user_id']);
                $listData['duration'] = $course_details[0]['duration'];
                $listData['fee'] = $course_details[0]['fee'];
                $listData['why_this'] = $course_details[0]['why_this'];
                $listData['description'] = $course_details[0]['description'];
                $listData['language'] = getName('languages', $course_details[0]['language_id']);
                $listData['image'] = base_url() . 'uploads/course/' . $course_details[0]['image'];
                $listData['certification_image'] = base_url() . 'uploads/certification/' . $course_details[0]['certification'];
                $listData['demo_video'] = base_url() . 'uploads/demo_video/' . $course_details[0]['demo_video'];
                $wheretopic = "course_id = $course_id AND user_id = $user_id";
                $topicdetailslist = $this->Common_model->getDataById('course_topics', $wheretopic);
                if (! empty($topicdetailslist)) {
                    foreach ($topicdetailslist as $topic) {
                        $topiclist['topic_id'] = $topic['id'];
                        $topiclist['topic_name'] = $topic['name'];
                        $topicdata[] = $topiclist;
                    }
                } else {
                    $topicdata = [];
                }
                $wherefeature = "course_id = $course_id AND user_id = $user_id";
                $featuredetailslist = $this->Common_model->getDataById('course_features', $wherefeature);
                if (! empty($featuredetailslist)) {
                    foreach ($featuredetailslist as $feature) {
                        $featurelist['feature_id'] = $feature['id'];
                        $featurelist['description'] = $feature['description'];
                        $featuredata[] = $featurelist;
                    }
                } else {
                    $featuredata = [];
                }
                $wherecurriculum = "course_id = $course_id AND user_id = $user_id";
                $curriculumdetailslist = $this->Common_model->getDataById('course_curriculums', $wherecurriculum);
                if (! empty($curriculumdetailslist)) {
                    foreach ($curriculumdetailslist as $curriculum) {
                        $curriculumlist['curriculum_id'] = $curriculum['id'];
                        $curriculumlist['name'] = $curriculum['name'];
                        $curriculumlist['pdf_file'] = base_url() . 'uploads/curriculum/' . $curriculum['pdf_file'];
                        $curriculumdata[] = $curriculumlist;
                    }
                } else {
                    $curriculumdata = [];
                }
                $wherefaq = "course_id = $course_id AND user_id = $user_id";
                $faqdetailslist = $this->Common_model->getDataById('course_faqs', $wherefaq);
                if (! empty($faqdetailslist)) {
                    foreach ($faqdetailslist as $faq) {
                        $faqlist['faq_id'] = $faq['id'];
                        $faqlist['question'] = $faq['question'];
                        $faqlist['answer'] = $faq['answer'];
                        $faqdata[] = $faqlist;
                    }
                } else {
                    $faqdata = [];
                }
                $whereproject = "course_id = $course_id AND user_id = $user_id";
                $projectdetailslist = $this->Common_model->getDataById('course_projects', $whereproject);
                if (! empty($projectdetailslist)) {
                    foreach ($projectdetailslist as $project) {
                        $projectlist['project_id'] = $project['id'];
                        $projectlist['name'] = $project['name'];
                        $projectlist['file'] = base_url() . 'uploads/project/' . $project['image'];
                        $projectdata[] = $projectlist;
                    }
                } else {
                    $projectdata = [];
                }
                $trainerdetailslist = $this->TrainingInstituteApimodel->getCourseTrainerData($course_id, $user_id);
                if (! empty($trainerdetailslist)) {
                    foreach ($trainerdetailslist as $trainer) {
                        $trainerlist['trainer_id'] = $trainer['id'];
                        $trainerlist['name'] = $trainer['name'];
                        $trainerlist['email'] = $trainer['email'];
                        $trainerlist['mobile'] = $trainer['mobile_no'];
                        $trainerlist['designation'] = $trainer['designation'];
                        $trainerlist['description'] = $trainer['description'];
                        $trainerlist['image'] = base_url() . 'uploads/profile/' . $trainer['image'];
                        $trainerlist['facebook'] = ($trainer['facebook']) ? $trainer['facebook'] : "";
                        $trainerlist['twitter'] = ($trainer['twitter']) ? $trainer['twitter'] : "";
                        $trainerlist['linkedin'] = ($trainer['linkedin']) ? $trainer['linkedin'] : "";
                        $trainerlist['youtube'] = ($trainer['youtube']) ? $trainer['youtube'] : "";
                        $trainerlist['instagram'] = ($trainer['instagram']) ? $trainer['instagram'] : "";
                        $trainerdata[] = $trainerlist;
                    }
                } else {
                    $trainerdata = [];
                }
                $listData['topic_list'] = $topicdata;
                $listData['feature_list'][] = $featuredata;
                $listData['curriculum_list'][] = $curriculumdata;
                $listData['faq_list'][] = $faqdata;
                $listData['project_list'][] = $projectdata;
                $listData['trainer_data'][] = $trainerdata;
                $resArr['result'] = 1;
                $resArr['message'] = "success";
                $resArr['data'][] = $listData;
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Failed";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_curriculum_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->curriculum_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_curriculums', $where);
            if ($result) {
                $file = $result[0]['pdf_file'];
                if ($file) {
                    unlink($this->curriculum . "" . $file);
                }
                $this->Common_model->delete('course_curriculums', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course Curriculum has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Curriculum not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_faq_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->faq_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_faqs', $where);
            if ($result) {
                $this->Common_model->delete('course_faqs', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course FAQ has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course FAQ not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_topic_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->topic_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_topics', $where);
            if ($result) {
                $file = $result[0]['image'];
                if ($file) {
                    unlink($this->topic . "" . $file);
                }
                $this->Common_model->delete('course_topics', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course Topic has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Topic not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_feature_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->feature_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_features', $where);
            if ($result) {
                $this->Common_model->delete('course_features', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course Feature has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Feature not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_project_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->project_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_projects', $where);
            if ($result) {
                $file = $result[0]['image'];
                if ($file) {
                    unlink($this->project . "" . $file);
                }
                $this->Common_model->delete('course_projects', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course Porject has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Porject not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function delete_course_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $curriculm_id = $requeset->course_id;
        if (! $user_id || ! $curriculm_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "id = $curriculm_id AND user_id = $user_id";
            $result = $this->Common_model->getDataById('course_details', $where);
            if ($result) {
                $file1 = $result[0]['certification'];
                $file2 = $result[0]['demo_video'];
                $file3 = $result[0]['image'];
                if ($file1) {
                    unlink($this->certificationt . "" . $file1);
                }
                if ($file2) {
                    unlink($this->demo_video . "" . $file2);
                }
                if ($file3) {
                    unlink($this->course . "" . $file3);
                }
                $this->Common_model->delete('course_details', $where);
                $resArr['result'] = 1;
                $resArr['message'] = "Course Details has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Details not found";
            }
        }
        echo $this->response($resArr, 200);
    }

    function update_course_details_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        
        if (! $course_id || ! $user_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
           
            if ($requeset->certification) {
                $explodedstring = explode(',', $requeset->certification);
                $explodedstring1 = explode('/', $explodedstring[0]);
                $explodedstring2 = explode(';', $explodedstring1[1]);
                $extension = $explodedstring2[0];
                $data = str_replace('data:image/' . $extension . ';base64,', '', $requeset->certification);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = FCPATH . '/uploads/certification/';
                $certificate_image_upload = strtotime(date("Y-m-d H:i:s")) . ".$extension";
                $filetosave = FCPATH . '/uploads/certification/' . $certificate_image_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            if ($requeset->image) {
                $explodedimage = explode(',', $requeset->image);
                $explodedimage1 = explode('/', $explodedimage[0]);
                $explodedimage2 = explode(';', $explodedimage1[1]);
                $extensionimage = $explodedimage2[0];
                $data = str_replace('data:image/' . $extensionimage . ';base64,', '', $requeset->image);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = FCPATH . '/uploads/course/';
                $image_upload = strtotime(date("Y-m-d H:i:s")) . ".$extensionimage";
                $filetosave = FCPATH . '/uploads/course/' . $image_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            if ($requeset->demo_video) {
                $data = str_replace('data:video/mp4;base64,', '', $requeset->demo_video);
                $data = str_replace(' ', '+', $data);
                $data = base64_decode($data);
                $url = $this->demo_video;
                $demo_video_upload = strtotime(date("Y-m-d H:i:s")) . ".mp4";
                $filetosave = FCPATH . '/uploads/demo_video/' . $demo_video_upload;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                $fileraw = curl_exec($ch);
                //echo $fileraw;die;
                curl_close($ch);
                $success = file_put_contents($filetosave, $data);
            }
            $where = "id = $course_id AND user_id = $user_id";
            $course_details = $this->Common_model->getDataById('course_details', $where);
            if ($course_details) {
                $updateArr = array(
                    'course_name' => ($requeset->course_name) ? $requeset->course_name : $course_details[0]['course_name'],
                    'duration' => ($requeset->duration) ? $requeset->duration : $course_details[0]['duration'],
                    'language_id' => ($requeset->language_id) ? $requeset->language_id : $course_details[0]['language_id'],
                    'certification' => ($requeset->certification) ? $certificate_image_upload : $course_details[0]['certification'],
                    'image' => ($requeset->image) ? $image_upload : $course_details[0]['image'],
                    'category_id' => ($requeset->category_id) ? $requeset->category_id : $course_details[0]['category_id'],
                    'sub_category_id' => ($requeset->sub_category_id) ? $requeset->sub_category_id : $course_details[0]['sub_category_id'],
                    'eligibility' => ($requeset->eligibility) ? $requeset->eligibility : $course_details[0]['eligibility'],
                    'fee' => ($requeset->fee) ? $requeset->fee : $course_details[0]['fee'],
                    'demo_video' => ($requeset->demo_video) ? $demo_video_upload : $course_details[0]['demo_video'],
                    'description' => ($requeset->description) ? $requeset->description : $course_details[0]['description'],
                    'why_this' => ($requeset->why_this) ? $requeset->why_this : $course_details[0]['why_this']
                );
                // print_r($insertArr);die;
                $result = $this->Common_model->update('course_details', $updateArr, $where);
                if ($result) {
                    $resArr['result'] = 1;
                    $resArr['course_id'] = $course_id;
                    $resArr['message'] = "Course Details has been updated successfully";
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Something went wrong. Please try again";
                }
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Course Details not found";
            }
        }
        echo $this->response($resArr, 200);
    }
    
    function change_password_post() {
        $resArr ['result'] = 0;
        $resArr ['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $password = $requeset->password;
        $old_password = $requeset->old_password;
        if (! $user_id || ! $old_password || !$password) {
            $resArr ['message'] = "Please pass all parameters";
        } else {
            // if (is_active($user_id)) {
            $pattern = "/^(?=.*[a-z])(?=\S{7,15})(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/";
            if(preg_match($pattern, $password)){
                $oldpass = md5($old_password);
                $whereCheck = "id = $user_id AND password ='$oldpass'";
                $checkUser =$this->Common_model->getDataById("users",$whereCheck);
                if($checkUser){
                    $insertArr = array (
                        'password' => md5($password)
                    );
                    $result = $this->Common_model->update ( 'users', $insertArr, $whereCheck );
                    if ($result) {
                        $resArr ['result'] = 1;
                        $resArr ['message'] = "Successfully updated password";
                    } else {
                        $resArr ['result'] = 0;
                        $resArr ['message'] = "Unable to update password. Please try again";
                    }
                } else {
                    $resArr ['result'] = 0;
                    $resArr ['message'] = "Old Password is wrong";
                }
            }else{
                $resArr ['result'] = 0;
                $resArr ['message'] = "Please enter valid password";
            }
            
            /* }else{
             $resArr['message'] = "Your account is inactive. Please contact with administration";
             } */
        }
        echo $this->response ( $resArr, 200 );
    }

    function upload_test_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $file = $requeset->file;
        $config['upload_path'] = $this->profile;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;
        
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (! $this->upload->do_upload($file)) {
            $error = array(
                'error' => $this->upload->display_errors()
            );
            
            print_r($error);
        } else {
            $data = array(
                'upload_data' => $this->upload->data()
            );
            print_r($data);
        }
    }
    
    function update_course_status_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $status = $requeset->status;
        if (! $course_id || ! $user_id || !$status) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $updateArr = array('status' => $status);
            $where = "id = $course_id AND user_id = $user_id";
            $result = $this->Common_model->update('course_details', $updateArr, $where);
            if($result){
                $resArr['result'] = 1;
                $resArr['message'] = "Status has been changed successfully";
            }else{
                $resArr['result'] = 0;
                $resArr['message'] = "Something wrong. Please try again";
            }
        }
        echo $this->response($resArr, 200);
    }
    
    function manage_batch_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $course_id = $requeset->course_id;
        $action = $requeset->action;
        if (! $course_id || !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
                $date = $requeset->batch_start_date;
                $type = $requeset->batch_type;
                $day_type = $requeset->day_type;
                $start_time = $requeset->start_time;
                $end_time = $requeset->end_time;
                $fee = $requeset->fee;
                if(!$date || !$day_type || !$type || !$start_time || !$end_time ||!$fee){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $insertArr = array(
                        'user_id' => $user_id,
                        'course_id' => $course_id,
                        'batch_start_date' => $date,
                        'batch_type' => $type,
                        'day_type' => $day_type,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'fee' => $fee,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('course_batches', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['course_id'] = $course_id;
                        $resArr['message'] = "Course Batch has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $date = $requeset->batch_start_date;
                $type = $requeset->batch_type;
                $day_type = $requeset->day_type;
                $start_time = $requeset->start_time;
                $end_time = $requeset->end_time;
                $fee = $requeset->fee;
                $id = $requeset->batch_id;
                if(!$id || !$date || !$day_type || !$type || !$start_time || !$end_time ||!$fee){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $updateArr = array(
                        'batch_start_date' => $date,
                        'batch_type' => $type,
                        'day_type' => $day_type,
                        'start_time' => $start_time,
                        'end_time' => $end_time,
                        'fee' => $fee
                    );
                    $whereupdate = "id = $id AND course_id = $course_id";
                    $result = $this->Common_model->update('course_batches', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['course_id'] = $course_id;
                        $resArr['message'] = "Course Batch has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='active'){
                $id = $requeset->batch_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $updateArr = array(
                        'status' => 1
                    );
                    $whereupdate = "id = $id AND course_id = $course_id";
                    $result = $this->Common_model->update('course_batches', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['course_id'] = $course_id;
                        $resArr['message'] = "Course Batch status has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='inactive'){
                $id = $requeset->batch_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $updateArr = array(
                        'status' => '0'
                    );
                    $whereupdate = "id = $id AND course_id = $course_id";
                    $result = $this->Common_model->update('course_batches', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['course_id'] = $course_id;
                        $resArr['message'] = "Course Batch status has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND course_id = $course_id AND status = 1";
                $result = $this->Common_model->getDataById('course_batches', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['batch_id'] = $res['id'];
                        $listData['course_id'] = $res['course_id'];
                        $listData['batch_start_date'] = $res['batch_start_date'];
                        $listData['type'] = $res['batch_type'];
                        $listData['day_type'] = $res['day_type'];
                        $listData['start_time'] = $res['start_time'];
                        $listData['end_time'] = $res['end_time'];
                        $listData['fee'] = $res['fee'];
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

    function manage_gallery_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$image = $request->image;
                if(!$image){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $data = str_replace('data:image/jpeg;base64,', '', $image);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = FCPATH . '/uploads/gallery/';
                    $uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
                    $filetosave = FCPATH . '/uploads/gallery/' . $uploadedimgProfile;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                    $insertArr = array(
                        'user_id' => $user_id,
                        'image' => ($image) ? $uploadedimgProfile : "",
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('gallery', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Gallery image has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
				$image = $request->image;
                $id = $request->gallery_id;
                if(!$image || !$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$whereupdate = "id = $id AND user_id = $user_id";
                    $gallerydata = $this->Common_model->getDataById('gallery', $whereupdate);
                    $data = str_replace('data:image/jpeg;base64,', '', $image);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = FCPATH . '/uploads/gallery/';
                    $uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
                    $filetosave = FCPATH . '/uploads/gallery/' . $uploadedimgProfile;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                    $updateArr = array(
                        'image' => ($image) ? $uploadedimgProfile : $gallerydata[0]['image'],
                    );
                    $result = $this->Common_model->update('gallery', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Gallery Image has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->gallery_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('gallery', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Gallery image has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('gallery', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['gallery_id'] = $res['id'];
                        $listData['image'] = ($res['image']) ? base_url() . "uploads/gallery/" . $res['image'] : "";
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

	function manage_news_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
       
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$image = $request->image;
				$title = $request->title;
				$desc = $request->desc;
                if(!$image || !$title || !$desc){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $data = str_replace('data:image/jpeg;base64,', '', $image);
                    $data = str_replace(' ', '+', $data);
                    $data = base64_decode($data);
                    $url = FCPATH . '/uploads/news/';
                    $uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
                    $filetosave = FCPATH . '/uploads/news/' . $uploadedimgProfile;
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    $fileraw = curl_exec($ch);
                    curl_close($ch);
                    $success = file_put_contents($filetosave, $data);
                    $insertArr = array(
                        'user_id' => $user_id,
                        'image' => ($image) ? $uploadedimgProfile : "",
						'title' => $title,
						'desc' => $desc,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('news', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "News has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $image = $request->image;
                $id = $request->news_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$whereupdate = "id = $id AND user_id = $user_id";
                    $newsdata = $this->Common_model->getDataById('news', $whereupdate);
					if($image){
						$data = str_replace('data:image/jpeg;base64,', '', $image);
						$data = str_replace(' ', '+', $data);
						$data = base64_decode($data);
						$url = FCPATH . '/uploads/news/';
						$uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
						$filetosave = FCPATH . '/uploads/news/' . $uploadedimgProfile;
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
						$fileraw = curl_exec($ch);
						curl_close($ch);
						$success = file_put_contents($filetosave, $data);
					}
                    $updateArr = array(
                        'image' => ($image) ? $uploadedimgProfile : $newsdata[0]['image'],
						'title' => ($request->title) ? $request->title :$newsdata[0]['title'],
						'desc' => ($request->desc) ? $request->desc :$newsdata[0]['desc'],
						'updated_at' => date("Y-m-d H:i:s")
                    );
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->update('news', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "News has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->news_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('news', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "News has been deleted successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('news', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['news_id'] = $res['id'];
                        $listData['image'] = ($res['image']) ? base_url() . "uploads/news/" . $res['image'] : "";
						$listData['title'] = $res['title'];
						$listData['desc'] = $res['desc'];
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

	function manage_facilities_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$desc = $request->desc;
                if( !$desc){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                    $insertArr = array(
                        'user_id' => $user_id,
						'description' => $desc,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('facilities', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Facility has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $id = $request->facility_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$whereupdate = "id = $id AND user_id = $user_id";
                    $newsdata = $this->Common_model->getDataById('facilities', $whereupdate);
					
                    $updateArr = array(
                        'description' => ($request->desc) ? $request->desc :$newsdata[0]['description'],
						'updated_at' => date("Y-m-d H:i:s")
                    );
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->update('facilities', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Facilities has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->facility_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('facilities', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Facilities has been deleted successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('facilities', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['facility_id'] = $res['id'];   
						$listData['desc'] = $res['description'];
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

	function manage_placement_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$desc = $request->message;
				$name = $request->name;
				$image = $request->image;
				$designation = $request->designation;
				$placement_date = $request->placement_date;
				$company = $request->company;
                if( !$desc || !$image || !$designation || !$placement_date || !$company || !$name){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$data = str_replace('data:image/jpeg;base64,', '', $image);
					$data = str_replace(' ', '+', $data);
					$data = base64_decode($data);
					$url = FCPATH . '/uploads/placement/';
					$uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
					$filetosave = FCPATH . '/uploads/placement/' . $uploadedimgProfile;
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
					$fileraw = curl_exec($ch);
					curl_close($ch);
					$success = file_put_contents($filetosave, $data);
                    $insertArr = array(
                        'user_id' => $user_id,
						'message' => $desc,
						'name' => $name,
						'image' => $uploadedimgProfile,
						'company' => $company,
						'designation' => $designation,
						'placement_date' => $placement_date,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('placements', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Placement has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $id = $request->placement_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$whereupdate = "id = $id AND user_id = $user_id";
                    $newsdata = $this->Common_model->getDataById('placements', $whereupdate);
					if($image){
						$data = str_replace('data:image/jpeg;base64,', '', $image);
						$data = str_replace(' ', '+', $data);
						$data = base64_decode($data);
						$url = FCPATH . '/uploads/placement/';
						$uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
						$filetosave = FCPATH . '/uploads/placement/' . $uploadedimgProfile;
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
						$fileraw = curl_exec($ch);
						curl_close($ch);
						$success = file_put_contents($filetosave, $data);
					}
                    $updateArr = array(
						'name' => ($request->name) ? $request->name :$newsdata[0]['name'],
                        'message' => ($request->message) ? $request->message :$newsdata[0]['message'],
						'image' => ($image) ? $uploadedimgProfile : $newsdata[0]['image'],
						'company' => ($request->company) ? $request->company :$newsdata[0]['company'],
						'designation' => ($request->designation) ? $request->designation :$newsdata[0]['designation'],
						'placement_date' => ($request->placement_date) ? $request->placement_date :$newsdata[0]['placement_date'],
						'updated_at' => date("Y-m-d H:i:s")
                    );
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->update('placements', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Placement has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->placement_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('placements', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Placements has been deleted successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('placements', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['placement_id'] = $res['id'];
						$listData['name'] = $res['name'];   
						$listData['company'] = $res['company'];
						$listData['designation'] = $res['designation'];   
						$listData['message'] = $res['message'];
						$listData['image'] = ($res['image']) ? base_url() . "uploads/placement/" . $res['image'] : "";
						$listData['placement_date'] = date('Y-m-d', strtotime($res['placement_date']));
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }
	function manage_job_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$desc = $request->description;
				$name = $request->name;
				$company = $request->company;
                if( !$desc ||!$company || !$name){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					
                    $insertArr = array(
                        'user_id' => $user_id,
						'description' => $desc,
						'name' => $name,
						'company' => $company,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('jobs', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Job has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $id = $request->job_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$whereupdate = "id = $id AND user_id = $user_id";
                    $newsdata = $this->Common_model->getDataById('jobs', $whereupdate);
					
                    $updateArr = array(
						'name' => ($request->name) ? $request->name :$newsdata[0]['name'],
                        'description' => ($request->description) ? $request->description :$newsdata[0]['description'],
						'company' => ($request->company) ? $request->company :$newsdata[0]['company'],
						'updated_at' => date("Y-m-d H:i:s")
                    );
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->update('jobs', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Jobs has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->job_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('jobs', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Jobs has been deleted successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('jobs', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['job_id'] = $res['id'];
						$listData['name'] = $res['name'];   
						$listData['company'] = $res['company'];
						$listData['description'] = $res['description'];  
						$listData['posted_date'] = date('Y-m-d', strtotime($res['created_at']));
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }
	function manage_kyc_post(){
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $request = json_decode($request_body);
        $user_id = $request->user_id;
        $action = $request->action;
        if ( !$action || ! $user_id ) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            if($action=='add'){
				$name = $request->name;
				$number = $request->number;
				$front_image = $request->front_image;
				$back_image = $request->back_image;
                if( !$name ||!$number || !$front_image || !$back_image){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					$data = str_replace('data:image/jpeg;base64,', '', $front_image);
					$data = str_replace(' ', '+', $data);
					$data = base64_decode($data);
					$url = FCPATH . '/uploads/kyc/';
					$uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
					$filetosave = FCPATH . '/uploads/kyc/' . $uploadedimgProfile;
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
					$fileraw = curl_exec($ch);
					curl_close($ch);
					$success = file_put_contents($filetosave, $data);

					$data1 = str_replace('data:image/jpeg;base64,', '', $back_image);
					$data1 = str_replace(' ', '+', $data1);
					$data1 = base64_decode($data);
					$url = FCPATH . '/uploads/kyc/';
					$uploadedimgProfile1 = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
					$filetosave1 = FCPATH . '/uploads/kyc/' . $uploadedimgProfile1;
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
					$fileraw = curl_exec($ch);
					curl_close($ch);
					$success = file_put_contents($filetosave1, $data1);
                    $insertArr = array(
                        'user_id' => $user_id,
						'doc_number' => $number,
						'name' => $name,
						'front_image' => $uploadedimgProfile,
						'back_image' => $uploadedimgProfile1,
                        'created_at' => date("Y-m-d H:i")
                    );
                    $result = $this->Common_model->insert('kyc', $insertArr);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Kyc has been added successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='edit'){
                $id = $request->kyc_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
					if($request->fornt_image){
						$data = str_replace('data:image/jpeg;base64,', '', $request->fornt_image);
						$data = str_replace(' ', '+', $data);
						$data = base64_decode($data);
						$url = FCPATH . '/uploads/kyc/';
						$uploadedimgProfile = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
						$filetosave = FCPATH . '/uploads/kyc/' . $uploadedimgProfile;
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
						$fileraw = curl_exec($ch);
						curl_close($ch);
						$success = file_put_contents($filetosave, $data);
					}
					
					if($request->back_image){
						$data1 = str_replace('data:image/jpeg;base64,', '', $request->back_image);
						$data1 = str_replace(' ', '+', $data1);
						$data1 = base64_decode($data);
						$url = FCPATH . '/uploads/kyc/';
						$uploadedimgProfile1 = strtotime(date("Y-m-d H:i:s")) . ".jpeg";
						$filetosave1 = FCPATH . '/uploads/kyc/' . $uploadedimgProfile1;
						$ch = curl_init($url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
						$fileraw = curl_exec($ch);
						curl_close($ch);
						$success = file_put_contents($filetosave1, $data1);
					}
					$whereupdate = "id = $id AND user_id = $user_id";
                    $newsdata = $this->Common_model->getDataById('kyc', $whereupdate);
					
                    $updateArr = array(
						'name' => ($request->name) ? $request->name :$newsdata[0]['name'],
                        'doc_number' => ($request->number) ? $request->number :$newsdata[0]['doc_number'],
						'fornt_image' => ($request->front_image) ? $uploadedimgProfile :$newsdata[0]['front_image'],
						'back_image' => ($request->back_image) ? $uploadedimgProfile1 :$newsdata[0]['back_image'],
						'updated_at' => date("Y-m-d H:i:s")
                    );
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->update('kyc', $updateArr, $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "Kyc has been updated successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else if($action=='delete'){
                $id = $request->kyc_id;
                if(!$id){
                    $resArr['message'] = "Please pass all mandatory parameters";
                }else{
                   
                    $whereupdate = "id = $id AND user_id = $user_id";
                    $result = $this->Common_model->delete('kyc', $whereupdate);
                    if ($result) {
                        $resArr['result'] = 1;
                        $resArr['message'] = "kyc has been deleted successfully";
                    } else {
                        $resArr['result'] = 0;
                        $resArr['message'] = "Something went wrong. Please try again";
                    }
                }
            }else{
                $where = "user_id = $user_id AND status = 1";
                $result = $this->Common_model->getDataById('kyc', $where);
                if ($result) {
                    foreach ($result as $res) {
                        $listData['kyc_id'] = $res['id'];
						$listData['name'] = $res['name'];   
						$listData['doc_number'] = $res['doc_number'];
						$listData['front_image'] = ($res['front_image']) ? base_url() . "uploads/kyc/" . $res['front_image'] : "";  
						$listData['back_image'] = ($res['back_image']) ? base_url() . "uploads/kyc/" . $res['back_image'] : "";
                        $resArr['result'] = 1;
                        $resArr['message'] = "success";
                        $resArr['data'][] = $listData;
                    }
                } else {
                    $resArr['result'] = 0;
                    $resArr['message'] = "Failed";
                }
            }
        }
        echo $this->response($resArr, 200);
    }

	function manage_city_address_time_post()
    {
        $resArr['result'] = 0;
        $resArr['message'] = "";
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
		$action = $requeset->action;
        if (! $action) {
            $resArr['message'] = "Please pass action parameters";
        } else {
            if ($action=='add') {
				$user_id = $requeset->user_id;
				$city = $requeset->city_id;
				$address = $requeset->address;
				$sunday_start_time = $requeset->sunday_start_time;
				$sunday_end_time = $requeset->sunday_end_time;
				$monday_start_time = $requeset->monday_start_time;
				$monday_end_time = $requeset->monday_end_time;
				$tuesday_start_time = $requeset->tuesday_start_time;
				$tuesday_end_time = $requeset->tuesday_end_time;
				$wednesday_start_time = $requeset->wednesday_start_time;
				$wednesday_end_time = $requeset->wednesday_end_time;
				$thursday_start_time = $requeset->thursday_start_time;
				$thursday_end_time = $requeset->thursday_end_time;
				$friday_start_time = $requeset->friday_start_time;
				$friday_end_time = $requeset->friday_end_time;
				$saturday_start_time = $requeset->saturday_start_time;
				$saturday_end_time = $requeset->saturday_end_time;
				$map_link = $requeset->map_link;
				if(!$user_id || !$city || !$address || !$map_link ){
					$resArr['message'] = "Please pass all parameter";
				}else{
					$insertArr = array(
						'user_id' => $user_id,
                        'city_id' => $city,
                        'address' => $address,
                        'map_link' => $map_link,
                        'sunday_start_time' => $sunday_start_time,
                        'sunday_end_time' => $sunday_end_time,
                        'monday_start_time' => $monday_start_time,
                        'monday_end_time' => $monday_end_time,
                        'tuesday_start_time' => $tuesday_start_time,
                        'tuesday_end_time' => $tuesday_end_time,
                        'wednesday_start_time' => $wednesday_start_time,
                        'wednesday_end_time' => $wednesday_end_time,
                        'thursday_start_time' => $thursday_start_time,
                        'thursday_end_time' => $thursday_end_time,
                        'friday_start_time' => $friday_start_time,
                        'friday_end_time' => $friday_end_time,
                        'saturday_start_time' => $saturday_start_time,
                        'saturday_end_time' => $saturday_end_time
                    );
                    $result = $this->Common_model->insert('city_address_time', $insertArr);
					if ($result) {
						$resArr['result'] = 1;
						$resArr['message'] = "Success. Your address has been added successfully";
					} else {
						$resArr['result'] = 0;
						$resArr['message'] = "Something went wrong. Please try again";
					}
				} 
                
            }
			if ($action=='update') {
				$city_address_time_id = $requeset->city_address_time_id;
				$user_id = $requeset->user_id;
				$city = $requeset->city_id;
				$address = $requeset->address;
				$sunday_start_time = $requeset->sunday_start_time;
				$sunday_end_time = $requeset->sunday_end_time;
				$monday_start_time = $requeset->monday_start_time;
				$monday_end_time = $requeset->monday_end_time;
				$tuesday_start_time = $requeset->tuesday_start_time;
				$tuesday_end_time = $requeset->tuesday_end_time;
				$wednesday_start_time = $requeset->wednesday_start_time;
				$wednesday_end_time = $requeset->wednesday_end_time;
				$thursday_start_time = $requeset->thursday_start_time;
				$thursday_end_time = $requeset->thursday_end_time;
				$friday_start_time = $requeset->friday_start_time;
				$friday_end_time = $requeset->friday_end_time;
				$saturday_start_time = $requeset->saturday_start_time;
				$saturday_end_time = $requeset->saturday_end_time;
				$map_link = $requeset->map_link;
				if(!$city_address_time_id || !$user_id || !$city || !$address || !$map_link ){
					$resArr['message'] = "Please pass all parameter";
				}else{
					$updateArr = array(
						'user_id' => $user_id,
                        'city_id' => $city,
                        'address' => $address,
                        'map_link' => $map_link,
                        'sunday_start_time' => $sunday_start_time,
                        'sunday_end_time' => $sunday_end_time,
                        'monday_start_time' => $monday_start_time,
                        'monday_end_time' => $monday_end_time,
                        'tuesday_start_time' => $tuesday_start_time,
                        'tuesday_end_time' => $tuesday_end_time,
                        'wednesday_start_time' => $wednesday_start_time,
                        'wednesday_end_time' => $wednesday_end_time,
                        'thursday_start_time' => $thursday_start_time,
                        'thursday_end_time' => $thursday_end_time,
                        'friday_start_time' => $friday_start_time,
                        'friday_end_time' => $friday_end_time,
                        'saturday_start_time' => $saturday_start_time,
                        'saturday_end_time' => $saturday_end_time
                    );
					$whereupdate = "id = $city_address_time_id AND user_id = $user_id";
                    $result = $this->Common_model->update('city_address_time', $updateArr,$whereupdate);
					if ($result) {
						$resArr['result'] = 1;
						$resArr['message'] = "Success. Your address has been updated successfully";
					} else {
						$resArr['result'] = 0;
						$resArr['message'] = "Something went wrong. Please try again";
					}
				} 
               
            }
			if($action == 'list'){
				$user_id = $requeset->user_id;
				$wherevid = "user_id = $user_id";
				$watcheddata = $this->Common_model->getDataById('city_address_time', $wherevid);
				if ($watcheddata) { 
					foreach($watcheddata as $result){
						$listData['city_address_time_id'] = $result['id'];
						$listData['user_id'] = $result['user_id'];
						$listData['address'] = $result['address'];
						$listData['city_id'] = $result['city_id'];
						$listData['city_name'] = getName('cities', $result['city_id']);
						$listData['sunday_start_time'] = $result['sunday_start_time'];
						$listData['sunday_end_time'] = $result['sunday_end_time'];
						$listData['monday_start_time'] = $result['monday_start_time'];
						$listData['monday_end_time'] = $result['monday_end_time'];
						$listData['tuesday_start_time'] = $result['tuesday_start_time'];
						$listData['tuesday_end_time'] = $result['tuesday_end_time'];
						$listData['wednesday_start_time'] = $result['wednesday_start_time'];
						$listData['wednesday_end_time'] = $result['wednesday_end_time'];
						$listData['thursday_start_time'] = $result['thursday_start_time'];
						$listData['thursday_end_time'] = $result['thursday_end_time'];
						$listData['friday_start_time'] = $result['friday_start_time'];
						$listData['friday_end_time'] = $result['friday_end_time'];
						$listData['saturday_start_time'] = $result['saturday_start_time'];
						$listData['saturday_end_time'] = $result['saturday_end_time'];
						$resArr['result'] = 1;
						$resArr['message'] = "success";
						$resArr['data'][] = $listData;
					}
					
					
				} else {
					$resArr['result'] = 0;
					$resArr['message'] = "No data found";
				}
			}
        }
        echo $this->response($resArr, 200);
    }
	function delete_trainer_post()
    {
        $request_body = file_get_contents('php://input');
        $requeset = json_decode($request_body);
        $user_id = $requeset->user_id;
        $trainer_id = $requeset->trainer_id;
        if (! $user_id || ! $trainer_id) {
            $resArr['message'] = "Please pass all mandatory parameters";
        } else {
            $where = "trainer_id = $trainer_id AND training_institute_user_id = $user_id";
            $result = $this->Common_model->delete('training_institute_trainers', $where);
			$where1 = "id = $trainer_id AND user_id = $trainer_id";
            $result1 = $this->Common_model->delete('trainers', $where1);
			$where2 = "id = $trainer_id";
            $result2 = $this->Common_model->delete('users', $where2);
            if ($result || $result1 || $result2) {
                $resArr['result'] = 1;
                $resArr['message'] = "Trainer has been deleted successfully";
            } else {
                $resArr['result'] = 0;
                $resArr['message'] = "Trainer has not be delete.";
            }
        }
        echo $this->response($resArr, 200);
    }

}
